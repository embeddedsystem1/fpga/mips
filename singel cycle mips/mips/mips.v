module mips(clk,aluresult,instr,scra,writedata,result,readdata,pc);

         input wire clk;
         
         output[31:0] aluresult,instr,scra,writedata,result,readdata,pc;

         wire[31:0] pc1,pc_pluse4,out_pcbranch;
         wire[31:0] result,signimm,scra,scrb,writedata,aluresult,readdata;
         wire[4:0] writereg;
         wire[2:0] alucontrol;
         wire pcscr,branch,zero,memtoreg,memwrite,aluscr,regdst,regwrite;

assign pcscr=branch & zero;


insteruction_memory ram0 (pc,instr);

data_memory ram1 (aluresult,writedata,memwrite,readdata,clk);

progrom_counter pc0 (pc1,pc,clk);

register_file regfile0 (instr[25:21],instr[20:16],writereg[4:0],result,regwrite,scra,writedata,clk);

pcbranch pc12 (signimm,pc_pluse4,out_pcbranch);

sign_extend sign0 (instr[15:0],signimm);

adder adder0 (pc,pc_pluse4);

alu alu0 (scra,scrb,alucontrol,aluresult,zero);

control_unit control0 (instr[31:26],instr[5:0],memtoreg,memwrite,branch,
alucontrol,aluscr,regdst,regwrite);

mux_32 mux0 (pc_pluse4,out_pcbranch,pcscr,pc1);

mux_32 mux1 (writedata,signimm,aluscr,scrb);

mux_32 mux2 (aluresult,readdata,memtoreg,result);

mux_5 mux3 (instr[20:16],instr[15:11],regdst,writereg);


endmodule

//1/////////////////////////////   insteruction_memory   //////////////////////////////

module insteruction_memory(a,rd);

  input[31:0] a;
 
 output reg[31:0] rd;

  (* ram_init_file = "memory.mif" *) reg[31:0] mem[63:0];

always@*
begin

rd<=mem[a];
end

endmodule

//2///////////////////////////////////   data memory   ////////////////////////////////////

module data_memory(a,wd,we,rd,clk);

input[31:0] wd;
     input we,clk;
     input[31:0] a;

 output reg[31:0] rd;

(* ram_init_file = "memory1.mif" *)reg [31:0] mem[63:0];

always@*

begin

if(we==0)
rd<=mem[a];

end

 always@(posedge clk)

begin
        if(we)
           mem[a] <= wd;

 end
 
 endmodule

//3///////////////////////////////////   progrom counter   /////////////////////////////////

module progrom_counter(pc1,pc,clk);

      input[31:0] pc1;
      input  clk;

   output reg[31:0] pc;

always@(posedge clk)

begin

pc<=pc1;

end

endmodule

//4////////////////////////////////////   register file   //////////////////////////////////

module register_file(a1,a2,a3,wd3,we3,rd1,rd2,clk);

       input[31:0] wd3;
       input[4:0] a1,a2,a3;
       input we3,clk;

      output[31:0] rd1,rd2;

reg[31:0] ram[31:0];

      

always@(posedge clk)

  begin

 if(we3)
 ram[a3]<=wd3;

 end
      assign rd1=ram[a1];
      assign rd2=ram[a2];

endmodule

//5///////////////////////////////////////   pcbranch   ////////////////////////////////////

module pcbranch(in_pcbranch0,in_pcbranch1,out_pcbranch);

    input[31:0] in_pcbranch0,in_pcbranch1;

  output[31:0] out_pcbranch;

            assign out_pcbranch=(in_pcbranch1+(in_pcbranch0<<2)); 

endmodule

//6/////////////////////////////////////////  sign extend  ////////////////////////////////

module sign_extend(in,out);

   input[15:0] in;

   output[31:0] out;

      assign out=in[15]?(in+32'hffff_0000):(in+32'h0000_0000);

endmodule

//7////////////////////////////////////   adder   ////////////////////////////////////////

module adder(in_adder,pcplus4);

      input[31:0] in_adder;

  output[31:0] pcplus4;

assign pcplus4=in_adder+32'h0000_0004;

endmodule

//8//////////////////////////////////////   alu    /////////////////////////////////////////

module alu(scra,scrb,alu_control,alu_result,zero);

  input[31:0] scra,scrb;
  input[2:0] alu_control;

        output reg[31:0] alu_result;
        output zero;

always@(scra or scrb or alu_control) 

 begin 

 case(alu_control)

       3'b010:alu_result=scra + scrb; 

       3'b110:alu_result=scra - scrb;

       3'b000:alu_result=scra & scrb;

       3'b001:alu_result=scra | scrb;

       3'b111:alu_result=scra > scrb;
default:alu_result=32'hxxxx_xxxx;
endcase

end
   assign zero=~|alu_result;

endmodule

//9////////////////////////////////////   control unit   /////////////////////////////////// 

module control_unit(opcode,funct,mem_to_reg,mem_write,branch,alu_control,alu_scr,
reg_dst,reg_write);

   input[5:0] opcode;
   input[5:0] funct;

              output[2:0] alu_control;
              output mem_to_reg,mem_write,branch,alu_scr,reg_dst,reg_write;
               
   wire[1:0] aluop;

assign reg_write=(~|opcode[5:0]) | (opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0]  );    
assign reg_dst=(~|opcode[5:0]);
assign alu_scr=(opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0])|(opcode[5]& ~opcode[4]& opcode[3]& ~opcode[2]& opcode[1]& opcode[0]  );
assign branch=(~opcode[5]& ~opcode[4]& ~opcode[3]& opcode[2]& ~opcode[1]& ~opcode[0]);
assign mem_write=(opcode[5]& ~opcode[4]& opcode[3]& ~opcode[2]& opcode[1]& opcode[0]);
assign mem_to_reg=(opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0]);
assign aluop[1]=reg_dst;
assign aluop[0]=branch;

assign alu_control[0]=(aluop[1] & funct[5]  & ~funct[4] & ~funct[3] & funct[2] & 
~funct[1] & funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);

assign alu_control[1]=(~|aluop[1:0])|(aluop[0])|(aluop[1] & funct[5] & (~|funct[4:0]))
|(aluop[1] & funct[5]  & ~funct[4]& ~funct[3] & ~funct[2] & funct[1] & ~funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);

assign alu_control[2]=aluop[0]|(aluop[1] & funct[5]  & ~funct[4]
& ~funct[3] & ~funct[2] & funct[1] & ~funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);


endmodule

//10//////////////////////////////////////  mux 32  /////////////////////////////////////////

module mux_32(in0_mux,in1_mux,sel_mux,out_mux);

       input[31:0] in0_mux,in1_mux;
       input sel_mux;

   output[31:0] out_mux;

  assign out_mux=sel_mux ? in1_mux:in0_mux;

endmodule  

//11////////////////////////////////////  mux 5   /////////////////////////////////////////// 

module mux_5(in0_mux,in1_mux,sel_mux,out_mux);

       input[4:0] in0_mux,in1_mux;
       input sel_mux;

    output[4:0] out_mux;

  assign out_mux=sel_mux ? in1_mux:in0_mux;

endmodule

  
