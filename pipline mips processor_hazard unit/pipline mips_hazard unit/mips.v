module mips(clk,aluoute,instrd,instrf,srcae,srcbe,resultw,readdataw,writedatae,signimme,signimmd,flushe);

         input wire clk;
         
         output[31:0] aluoute,instrd,srcae,srcbe,resultw,readdataw,writedatae,signimme,signimmd,instrf;

         output flushe;         

         wire[63:0] inreg0,outreg0;

         wire[118:0] inreg1,outreg1;

         wire[71:0] inreg2,outreg2;

         wire[70:0] inreg3,outreg3;

         wire[31:0] pc1,pcf,pcpluse4f,pcpluse4d,pcbranchd,brancheq0,brancheq1;

         wire[31:0] instrf,instrd,resultw,signimmd,signimme,srcad,srcae,srcbe
         ,writedatad0,writedatad1,writedatae,writedatam
         ,aluoute,aluoutm,aluoutw,readdatam,readdataw,writedatae0,writedatae1;

         wire[4:0] writerege,writeregm,writeregw,rtd,rte,rdd,rde,rsd,rse;

         wire[2:0] alucontrold,alucontrole;

         wire[1:0] forwardae,forwardbe;

         wire pcsrcd,branchd,branche,branchm,zeroe,zerom,memtoregd,memtorege,memtoregm
         ,memtoregw,memwrited,memwritee,memwritem,
         alusrcd,alusrce,regdstd,regdste,regwrited,regwritee,regwritem,regwritew;
         
         wire stallf,stalld,equald,flushe,flushet,forwardad,forwardbd;
assign flushet=0;

assign inreg0[31:0]=instrf;
assign inreg0[63:32]=pcpluse4f;

assign instrd=outreg0[31:0];
assign pcpluse4d=outreg0[63:32];
assign rtd=instrd[20:16];
assign rdd=instrd[15:11];
assign rsd=instrd[25:21];



assign inreg1[118]=regwrited;
assign inreg1[117]=memtoregd;
assign inreg1[116]=memwrited;
assign inreg1[115:113]=alucontrold;
assign inreg1[112]=alusrcd;
assign inreg1[111]=regdstd;
assign inreg1[110:79]=writedatad1;
assign inreg1[78:47]=writedatad0;
assign inreg1[46:42]=rsd;
assign inreg1[41:37]=rtd;
assign inreg1[36:32]=rdd;
assign inreg1[31:0]=signimmd;

assign regwritee=outreg1[118];
assign memtorege=outreg1[117];
assign memwritee=outreg1[116];
assign alucontrole=outreg1[115:113];
assign alusrce=outreg1[112];
assign regdste=outreg1[111];
assign writedatae1=outreg1[110:79];
assign writedatae0=outreg1[78:47];
assign rse=outreg1[46:42];
assign rte=outreg1[41:37];
assign rde=outreg1[36:32];
assign signimme=outreg1[31:0];



assign inreg2[71]=regwritee;
assign inreg2[70]=memtorege;
assign inreg2[69]=memwritee;
assign inreg2[68:37]=aluoute;
assign inreg2[36:5]=writedatae;
assign inreg2[4:0]=writerege;


assign regwritem=outreg2[71];
assign memtoregm=outreg2[70];
assign memwritem=outreg2[69];
assign aluoutm=outreg2[68:37];
assign writedatam=outreg2[36:5];
assign writeregm=outreg2[4:0];




assign inreg3[70]=regwritem;
assign inreg3[69]=memtoregm;
assign inreg3[68:37]=aluoutm;
assign inreg3[36:5]=readdatam;
assign inreg3[4:0]=writeregm;

assign regwritew=outreg3[70];
assign memtoregw=outreg3[69];
assign aluoutw=outreg3[68:37];
assign readdataw=outreg3[36:5];
assign writeregw=outreg3[4:0];



assign pcsrcd=branchd & equald;


insteruction_memory ram0 (pcf,instrf);

data_memory ram1 (aluoutm,writedatam,memwritem,readdatam,clk);

progrom_counter pc0 (pc1,pcf,clk,stallf);

register_file regfile0 (instrd[25:21],instrd[20:16],writeregw,resultw,regwritew,writedatad1,writedatad0,clk);

register64 reg0(inreg0,outreg0,clk,stalld,pcsrcd);

register119 reg1(inreg1,outreg1,clk,flushet);

register72 reg2(inreg2,outreg2,clk);

register71 reg3(inreg3,outreg3,clk);

pcbranch pc12 (signimmd,pcpluse4d,pcbranchd);

sign_extend sign0 (instrd[15:0],signimmd);

adder adder0 (pcf,pcpluse4f);

alu alu0 (srcae,srcbe,alucontrole,aluoute);

control_unit control0 (instrd[31:26],instrd[5:0],memtoregd,memwrited,branchd,
alucontrold,alusrcd,regdstd,regwrited);

mux_32 mux0 (pcpluse4f,pcbranchd,pcsrcd,pc1);

mux_32 mux1 (writedatae,signimme,alusrce,srcbe);

mux_32 mux2 (aluoutw,readdataw,memtoregw,resultw);

mux_5 mux3 (rte,rde,regdste,writerege);

mux_32 mux4 (writedatad1,aluoutm,forwardad,brancheq0);

mux_32 mux5 (writedatad0,aluoutm,forwardbd,brancheq1);

mux_32_3 mux6 (writedatae1,resultw,aluoutm,forwardae,srcae);

mux_32_3 mux7 (writedatae0,resultw,aluoutm,forwardbe,writedatae);

comparator cmp0 (brancheq0,brancheq1,equald);

hazard hazard0 (stallf,stalld,branchd,forwardad,forwardbd,rsd,rtd,flushe,rse,rte,
forwardae,forwardbe,writerege,memtorege,regwritee,writeregm,memtoregm,
regwritem,writeregw,regwritew);


endmodule

//1////////////////////////////   insteruction_memory   //////////////////////////////

module insteruction_memory(a,rd);

  input[31:0] a;
 
 output reg[31:0] rd;

  (* ram_init_file = "memory.mif" *) reg[31:0] mem[63:0];

always@*
begin

rd<=mem[a];
end

endmodule

//2//////////////////////////////////   data memory   ////////////////////////////////////

module data_memory(a,wd,we,rd,clk);

input[31:0] wd;
     input we,clk;
     input[31:0] a;

 output reg[31:0] rd;

(* ram_init_file = "memory1.mif" *)reg [31:0] mem[63:0];

always@*

begin

rd<=mem[a];

end

 always@(posedge clk)

begin
        if(we)
           mem[a] <= wd;

 end
 
 endmodule

//3/////////////////////////////////   progrom counter   /////////////////////////////////

module progrom_counter(pc1,pc,clk,en);

      input[31:0] pc1;
      input  en;
      input  clk;

   output reg[31:0] pc;

always@(posedge clk)

begin

  if(en==0)
  pc<=pc1;

end

 endmodule

//4//////////////////////////////////   register file   //////////////////////////////////

module register_file(a1,a2,a3,wd3,we3,rd1,rd2,clk);

       input[31:0] wd3;
       input[4:0] a1,a2,a3;
       input we3,clk;

      output[31:0] rd1,rd2;

reg[31:0] ram[31:0];

            assign rd1=ram[a1];
            assign rd2=ram[a2];

always@(posedge clk)

  begin

 if(we3)
 ram[a3]<=wd3;

 end


endmodule

//5///////////////////////////////////   pcbranch   ////////////////////////////////////

module pcbranch(in_pcbranch0,in_pcbranch1,out_pcbranch);

    input[31:0] in_pcbranch0,in_pcbranch1;

  output[31:0] out_pcbranch;

            assign out_pcbranch=(in_pcbranch1+(in_pcbranch0<<2)); 

endmodule

//6/////////////////////////////////////////  sign extend  ///////////////////////////////

module sign_extend(in,out);

   input[15:0] in;

   output[31:0] out;

      assign out=in[15]?(in+32'hffff_0000):(in+32'h0000_0000);

endmodule

//7////////////////////////////////////   adder   ///////////////////////////////////////

module adder(in_adder,pcplus4);

      input[31:0] in_adder;

  output[31:0] pcplus4;

assign pcplus4=in_adder+32'h0000_0004;

endmodule

//8//////////////////////////////////////   alu    ///////////////////////////////////////

module alu(srca,srcb,alu_control,alu_result);

  input[31:0] srca,srcb;
  input[2:0] alu_control;

        output reg[31:0] alu_result;

always@(srca or srcb or alu_control) 

 begin 

 case(alu_control)

       3'b010:alu_result=srca + srcb; 

       3'b110:alu_result=srca - srcb;

       3'b000:alu_result=srca & srcb;

       3'b001:alu_result=srca | srcb;

       3'b111:alu_result=srca > srcb;
default:alu_result=32'hxxxx_xxxx;
endcase

end
  
endmodule

//9////////////////////////////////////   control unit   ///////////////////////////////// 

module control_unit(opcode,funct,mem_to_reg,mem_write,branch,alu_control,alu_src,
reg_dst,reg_write);

   input[5:0] opcode;
   input[5:0] funct;

              output[2:0] alu_control;
              output mem_to_reg,mem_write,branch,alu_src,reg_dst,reg_write;
               
   wire[1:0] aluop;

assign reg_write=(~|opcode[5:0]) | (opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0]  );    
assign reg_dst=(~|opcode[5:0]);
assign alu_src=(opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0])|(opcode[5]& ~opcode[4]& opcode[3]& ~opcode[2]& opcode[1]& opcode[0]  );
assign branch=(~opcode[5]& ~opcode[4]& ~opcode[3]& opcode[2]& ~opcode[1]& ~opcode[0]);
assign mem_write=(opcode[5]& ~opcode[4]& opcode[3]& ~opcode[2]& opcode[1]& opcode[0]);
assign mem_to_reg=(opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0]);
assign aluop[1]=reg_dst;
assign aluop[0]=branch;

assign alu_control[0]=(aluop[1] & funct[5]  & ~funct[4] & ~funct[3] & funct[2] & 
~funct[1] & funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);

assign alu_control[1]=(~|aluop[1:0])|(aluop[0])|(aluop[1] & funct[5] & (~|funct[4:0]))
|(aluop[1] & funct[5]  & ~funct[4]& ~funct[3] & ~funct[2] & funct[1] & ~funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);

assign alu_control[2]=aluop[0]|(aluop[1] & funct[5]  & ~funct[4]
& ~funct[3] & ~funct[2] & funct[1] & ~funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);


endmodule

//10//////////////////////////////////////  mux 32  ///////////////////////////////////////

module mux_32(in0_mux,in1_mux,sel_mux,out_mux);

       input[31:0] in0_mux,in1_mux;
       input sel_mux;

   output[31:0] out_mux;

  assign out_mux=sel_mux ? in1_mux:in0_mux;

endmodule  

//11////////////////////////////////////  mux 5   //////////////////////////////////////// 

module mux_5(in0_mux,in1_mux,sel_mux,out_mux);

       input[4:0] in0_mux,in1_mux;
       input sel_mux;

    output[4:0] out_mux;

  assign out_mux=sel_mux ? in1_mux:in0_mux;

endmodule

//12//////////////////////////////// register 64 bits ////////////////////////////////////

module register64(in,out,clk,en,clr);

   input[63:0] in;
   input en;
   input clr;
   input clk;

     output reg[63:0] out;
    
always@(posedge clk or  posedge clr )

begin 
if(clr==1)
  out<=0;

 else if(en==0)
  out<=in;
  
  
  end

endmodule

//13/////////////////////////////// register 119 bits ////////////////////////////////////

module register119(in,out,clk,clr);

   input[118:0] in;
   input clr;
   input clk;

     output reg[118:0] out;
    
always@(posedge clk or posedge clr )

begin

 if(clr==1)
  out<=0;
else

  out<=in; 

end

endmodule

//14//////////////////////////// register 72 bits ////////////////////////////////////////

module register72(in,out,clk);

  input[71:0] in;
  input clk;

     output reg[71:0] out;

always@(posedge clk)

begin
 
 out<=in;

end

endmodule

//15///////////////////////////// register 71 bits ///////////////////////////////////////

module register71(in,out,clk);

   input[70:0] in;
   input clk;

     output reg[70:0] out;
    
always@(posedge clk )

begin 

out<=in;

end

endmodule

//16///////////////////////////// mux 32_3 ////////////////////////////////////////////  

 module mux_32_3(in0,in1,in2,sel,out);

        input[31:0] in0,in1,in2;
        input[1:0] sel;

  output reg[31:0] out;


always@*

   begin

case(sel)

   2'b00:out=in0;
   2'b01:out=in1;
   2'b10:out=in2;

default:out=32'hxxxx_xxxx;

 endcase

end

endmodule

//17///////////////////////// comparator ///////////////////////////////////////////

module comparator(in0,in1,out);

  input[31:0] in0,in1;

  output reg out;

always@*

  begin

    if(in0==in1)
    out<=1'b1;

  else
  out<=1'b0;

 end

 endmodule

//18///////////////////////////// hazard unit /////////////////////////////////////////

module hazard(stallf,stalld,branchd,forwardad,forwardbd,rsd,rtd,flushe,rse,rte,
forwardae,forwardbe,writerege,memtorege,regwritee,writeregm,memtoregm,
regwritem,writeregw,regwritew);

  input branchd,memtorege,regwritee,memtoregm,regwritem,regwritew;
  
  input[4:0] rsd,rtd,rse,rte,writerege,writeregm,writeregw;
  
       output reg stallf,stalld,forwardad,forwardbd,flushe;
       output reg[1:0]  forwardae,forwardbe;
     
 
always@*

  begin
 
if((rse!=0)&(rse==writeregm)& regwritem)
  forwardae=2'b10;

else if((rse!=0)&(rse==writeregw)& regwritew)
  forwardae=2'b01;

else
 forwardae=2'b00;

if((rte!=0)&(rte==writeregm)& regwritem)
  forwardbe=2'b10;

else if((rte!=0)&(rte==writeregw)& regwritew)
  forwardbe=2'b01;

else
 forwardbe=2'b00;

	
	forwardad=((rsd!=0)&(rsd==writeregm)& regwritem);
	forwardbd=((rsd!=0)&(rtd==writeregm)& regwritem);
	
	
	stallf=((branchd & regwritee) &((writerege==rsd)|(writerege==rtd)))|
	((branchd & memtoregm) &((writeregm==rsd)|(writeregm==rtd)))|
	(((rsd==rte)|(rtd==rte))& memtorege);
	
	stalld=stallf;
	flushe=stallf;
	
end

endmodule