module mips(clk,aluoute,instrd,srcae,srcbe,resultw,readdataw,writedatae,signimmd);

         input wire clk;
         
         output[31:0] aluoute,instrd,srcae,srcbe,resultw,readdataw,writedatae,signimmd;

         wire[63:0] inreg0,outreg0;

         wire[146:0] inreg1,outreg1;

         wire[105:0] inreg2,outreg2;

         wire[70:0] inreg3,outreg3;

         wire[31:0] pc1,pcf,pcpluse4f,pcpluse4d,pcpluse4e,pcbranche,pcbranchm;

         wire[31:0] instrf,instrd,resultw,signimmd,signimme,srcad,srcae,srcbe
         ,writedatad,writedatae,writedatam
         ,aluoute,aluoutm,aluoutw,readdatam,readdataw;

         wire[4:0] writerege,writeregm,writeregw,rtd,rte,rdd,rde;

         wire[2:0] alucontrold,alucontrole;

         wire pcsrcm,branchd,branche,branchm,zeroe,zerom,memtoregd,memtorege,memtoregm
         ,memtoregw,memwrited,memwritee,memwritem,
         alusrcd,alusrce,regdstd,regdste,regwrited,regwritee,regwritem,regwritew;

assign inreg0[63:32]=instrf;
assign inreg0[31:0]=pcpluse4f;

assign instrd=outreg0[63:32];
assign pcpluse4d=outreg0[31:0];
assign rtd=instrd[20:16];
assign rdd=instrd[15:11];



assign inreg1[146]=regwrited;
assign inreg1[145]=memtoregd;
assign inreg1[144]=memwrited;
assign inreg1[143]=branchd;
assign inreg1[142:140]=alucontrold;
assign inreg1[139]=alusrcd;
assign inreg1[138]=regdstd;
assign inreg1[137:106]=srcad;
assign inreg1[105:74]=writedatad;
assign inreg1[73:69]=rtd;
assign inreg1[68:64]=rdd;
assign inreg1[63:32]=signimmd;
assign inreg1[31:0]=pcpluse4d;

assign regwritee=outreg1[146];
assign memtorege=outreg1[145];
assign memwritee=outreg1[144];
assign branche=outreg1[143];
assign alucontrole=outreg1[142:140];
assign alusrce=outreg1[139];
assign regdste=outreg1[138];
assign srcae=outreg1[137:106];
assign writedatae=outreg1[105:74];
assign rte=outreg1[73:69];
assign rde=outreg1[68:64];
assign signimme=outreg1[63:32];
assign pcpluse4e=outreg1[31:0];



assign inreg2[105]=regwritee;
assign inreg2[104]=memtorege;
assign inreg2[103]=memwritee;
assign inreg2[102]=branche;
assign inreg2[101]=zeroe;
assign inreg2[100:69]=aluoute;
assign inreg2[68:37]=writedatae;
assign inreg2[36:32]=writerege;
assign inreg2[31:0]=pcbranche;

assign regwritem=outreg2[105];
assign memtoregm=outreg2[104];
assign memwritem=outreg2[103];
assign branchm=outreg2[102];
assign zerom=outreg2[101];
assign aluoutm=outreg2[100:69];
assign writedatam=outreg2[68:37];
assign writeregm=outreg2[36:32];
assign pcbranchm=outreg2[31:0];



assign inreg3[70]=regwritem;
assign inreg3[69]=memtoregm;
assign inreg3[68:37]=aluoutm;
assign inreg3[36:5]=readdatam;
assign inreg3[4:0]=writeregm;

assign regwritew=outreg3[70];
assign memtoregw=outreg3[69];
assign aluoutw=outreg3[68:37];
assign readdataw=outreg3[36:5];
assign writeregw=outreg3[4:0];



assign pcsrcm=branchm & zerom;


insteruction_memory ram0 (pcf,instrf);

data_memory ram1 (aluoutm,writedatam,memwritem,readdatam,clk);

progrom_counter pc0 (pc1,pcf,clk);

register_file regfile0 (instrd[25:21],instrd[20:16],writeregw,resultw,regwritew,srcad,writedatad,clk);

register64 reg0(inreg0,outreg0,clk);

register147 reg1(inreg1,outreg1,clk);

register106 reg2(inreg2,outreg2,clk);

register71 reg3(inreg3,outreg3,clk);

pcbranch pc12 (signimme,pcpluse4e,pcbranche);

sign_extend sign0 (instrd[15:0],signimmd);

adder adder0 (pcf,pcpluse4f);

alu alu0 (srcae,srcbe,alucontrole,aluoute,zeroe);

control_unit control0 (instrd[31:26],instrd[5:0],memtoregd,memwrited,branchd,
alucontrold,alusrcd,regdstd,regwrited);

mux_32 mux0 (pcpluse4f,pcbranchm,pcsrcm,pc1);

mux_32 mux1 (writedatae,signimme,alusrce,srcbe);

mux_32 mux2 (aluoutw,readdataw,memtoregw,resultw);

mux_5 mux3 (rte,rde,regdste,writerege);


endmodule

//1/////////////////////////////   insteruction_memory   //////////////////////////////

module insteruction_memory(a,rd);

  input[31:0] a;
 
 output reg[31:0] rd;

  (* ram_init_file = "memory.mif" *) reg[31:0] mem[63:0];

always@*
begin

rd<=mem[a];
end

endmodule

//2///////////////////////////////////   data memory   ////////////////////////////////////

module data_memory(a,wd,we,rd,clk);

input[31:0] wd;
     input we,clk;
     input[31:0] a;

 output reg[31:0] rd;

(* ram_init_file = "memory1.mif" *)reg [31:0] mem[63:0];

always@*

begin

rd<=mem[a];

end

 always@(posedge clk)

begin
        if(we)
           mem[a] <= wd;

 end
 
 endmodule

//3///////////////////////////////////   progrom counter   /////////////////////////////////

module progrom_counter(pc1,pc,clk);

      input[31:0] pc1;
      input  clk;

   output reg[31:0] pc;

always@(posedge clk)

begin

pc<=pc1;

end

endmodule

//4////////////////////////////////////   register file   //////////////////////////////////

module register_file(a1,a2,a3,wd3,we3,rd1,rd2,clk);

       input[31:0] wd3;
       input[4:0] a1,a2,a3;
       input we3,clk;

      output[31:0] rd1,rd2;

reg[31:0] ram[31:0];

            assign rd1=ram[a1];
            assign rd2=ram[a2];

always@(posedge clk)

  begin

 if(we3)
 ram[a3]<=wd3;

 end


endmodule

//5///////////////////////////////////////   pcbranch   ////////////////////////////////////

module pcbranch(in_pcbranch0,in_pcbranch1,out_pcbranch);

    input[31:0] in_pcbranch0,in_pcbranch1;

  output[31:0] out_pcbranch;

            assign out_pcbranch=(in_pcbranch1+(in_pcbranch0<<2)); 

endmodule

//6/////////////////////////////////////////  sign extend  ////////////////////////////////

module sign_extend(in,out);

   input[15:0] in;

   output[31:0] out;

      assign out=in[15]?(in+32'hffff_0000):(in+32'h0000_0000);

endmodule

//7////////////////////////////////////   adder   ////////////////////////////////////////

module adder(in_adder,pcplus4);

      input[31:0] in_adder;

  output[31:0] pcplus4;

assign pcplus4=in_adder+32'h0000_0004;

endmodule

//8//////////////////////////////////////   alu    /////////////////////////////////////////

module alu(srca,srcb,alu_control,alu_result,zero);

  input[31:0] srca,srcb;
  input[2:0] alu_control;

        output reg[31:0] alu_result;
        output zero;

always@(srca or srcb or alu_control) 

 begin 

 case(alu_control)

       3'b010:alu_result=srca + srcb; 

       3'b110:alu_result=srca - srcb;

       3'b000:alu_result=srca & srcb;

       3'b001:alu_result=srca | srcb;

       3'b111:alu_result=srca > srcb;
default:alu_result=32'hxxxx_xxxx;
endcase

end
   assign zero=~|alu_result;

endmodule

//9////////////////////////////////////   control unit   /////////////////////////////////// 

module control_unit(opcode,funct,mem_to_reg,mem_write,branch,alu_control,alu_src,
reg_dst,reg_write);

   input[5:0] opcode;
   input[5:0] funct;

              output[2:0] alu_control;
              output mem_to_reg,mem_write,branch,alu_src,reg_dst,reg_write;
               
   wire[1:0] aluop;

assign reg_write=(~|opcode[5:0]) | (opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0]  );    
assign reg_dst=(~|opcode[5:0]);
assign alu_src=(opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0])|(opcode[5]& ~opcode[4]& opcode[3]& ~opcode[2]& opcode[1]& opcode[0]  );
assign branch=(~opcode[5]& ~opcode[4]& ~opcode[3]& opcode[2]& ~opcode[1]& ~opcode[0]);
assign mem_write=(opcode[5]& ~opcode[4]& opcode[3]& ~opcode[2]& opcode[1]& opcode[0]);
assign mem_to_reg=(opcode[5]& ~opcode[4]& ~opcode[3]& ~opcode[2]& opcode[1]& opcode[0]);
assign aluop[1]=reg_dst;
assign aluop[0]=branch;

assign alu_control[0]=(aluop[1] & funct[5]  & ~funct[4] & ~funct[3] & funct[2] & 
~funct[1] & funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);

assign alu_control[1]=(~|aluop[1:0])|(aluop[0])|(aluop[1] & funct[5] & (~|funct[4:0]))
|(aluop[1] & funct[5]  & ~funct[4]& ~funct[3] & ~funct[2] & funct[1] & ~funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);

assign alu_control[2]=aluop[0]|(aluop[1] & funct[5]  & ~funct[4]
& ~funct[3] & ~funct[2] & funct[1] & ~funct[0])|
(aluop[1] & funct[5]  & ~funct[4] & funct[3] & ~funct[2] & funct[1] & ~funct[0]);


endmodule

//10//////////////////////////////////////  mux 32  /////////////////////////////////////////

module mux_32(in0_mux,in1_mux,sel_mux,out_mux);

       input[31:0] in0_mux,in1_mux;
       input sel_mux;

   output[31:0] out_mux;

  assign out_mux=sel_mux ? in1_mux:in0_mux;

endmodule  

//11////////////////////////////////////  mux 5   /////////////////////////////////////////// 

module mux_5(in0_mux,in1_mux,sel_mux,out_mux);

       input[4:0] in0_mux,in1_mux;
       input sel_mux;

    output[4:0] out_mux;

  assign out_mux=sel_mux ? in1_mux:in0_mux;

endmodule

//12//////////////////////////////// register 64 bits ///////////////////////////////////////

module register64(in,out,clk);

   input[63:0] in;
   input clk;

     output reg[63:0] out;
    
always@(posedge clk )

begin 

out<=in;

end

endmodule

//13/////////////////////////////// register 147 bits ///////////////////////////////////////

module register147(in,out,clk);

   input[146:0] in;
   input clk;

     output reg[146:0] out;
    
always@(posedge clk )

begin 

out<=in;

end

endmodule

//14//////////////////////////// register 106 bits ///////////////////////////////////////////

module register106(in,out,clk);

  input[105:0] in;
  input clk;

     output reg[105:0] out;

always@(posedge clk)

begin
 
 out<=in;

end

endmodule

//15///////////////////////////// register 71 bits ///////////////////////////////////////////

module register71(in,out,clk);

   input[70:0] in;
   input clk;

     output reg[70:0] out;
    
always@(posedge clk )

begin 

out<=in;

end

endmodule

  
